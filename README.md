# Screenshooter <sup><sub>ManiaPlanet Trackmania</sub></sup>
Enhance in-game screenshot feature and enable custom resolutions, 360 panoramas and more.

## Features
* Custom resolution (from *16x16* to *8K* and more)
* 3 output formats 
    * `.webp` (slow, invisible compression, small file size)
    * `.jpg` (fast, visible compression, smallest file size)
    * `.tga` (fastest, lossless, huge file size)
* 360 Panorama (equirectangular)
* Tiling (for super-resolution screenshots)
* Sequential capturing (99 frames maximum, captured each 10th of a second)

## Limitations
* 99 screenshots in the folder will cause overwriting (starting from the first one)
* Extreme resolutions can cause crashes (16K and more)
* Tiling is available only in *ManiaPlanet* (missing top row)
* Panorama mode
    * Available only in *ManiaPlanet* (*Trackmania2020* crashes)
    * Most maps cause flipped tiles (fixable using *Fix Tiles* option sacrificing quality)
    * TARGA (`.tga`) flips *Red* and *Blue* color channels
    * Noticeable seams due to fake lighting

## Download
* [Openplanet](https://openplanet.dev/plugin/screenshooter)
* [Releases](https://gitlab.com/fentrasLABS/openplanet/screenshooter/-/releases)

## Media
![Plugin Windows](_git/1.png)
[![CCP#12 - [PF] Satelite Delivery in 360](_git/2_preview.png)](_git/2.png)
[![Tiling feature in 64K](_git/3_preview.png)](_git/3.webm)
[![Sequential Capturing feature in 360](_git/4_preview.png)](_git/4.webm)

## Credits
Project icon provided by [Fork Awesome](https://forkaweso.me/)
